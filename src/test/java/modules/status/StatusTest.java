package modules.status;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

@DisplayName("Buscar o status da aplicação")
public class StatusTest {
    @Test
    @DisplayName("Testar se a aplicação está rodando")
    public void testTestarAplicacao(){
        baseURI = "https://dummyjson.com";

        when()
            .get("/test")
        .then()
            .statusCode(200)
            .body("status", equalTo("ok"))
            .body("method", equalTo("GET"));
    }
}
