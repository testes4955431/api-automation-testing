package modules.users;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pojo.UserPojo;

import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Testes de API Rest do modulo de Usuario")
public class UserTest {

    @BeforeEach
    public void beforeEach(){
        baseURI = "https://dummyjson.com";
    }

    @Test
    @DisplayName("Validar a listagem dos usuários")
    public void testValidarListagemDosUsuarios(){
        Response response = given()
            .when()
                .get("/users")
            .then()
                .assertThat()
                .statusCode(200)
                .extract()
                .response();

        JsonPath jsonPath = response.jsonPath();
        List<Map<String, ?>> users = jsonPath.getList("users");

        for(Map<String, ?> user : users) {
            assertTrue(user.containsKey("id"), "Faltando o 'id' no usuario: " + user);
            assertTrue(user.containsKey("firstName"), "Faltando o 'firstName' no usuario: " + user);
            assertTrue(user.containsKey("lastName"), "Faltando o 'lastName' no usuario: " + user);
            assertTrue(user.containsKey("maidenName"), "Faltando o 'maidenName' no usuario: " + user);
            assertTrue(user.containsKey("age"), "Faltando o 'age' no usuario: " + user);
            assertTrue(user.containsKey("gender"), "Faltando o 'gender' no usuario: " + user);
            assertTrue(user.containsKey("email"), "Faltando o 'email' no usuario: " + user);
            assertTrue(user.containsKey("phone"), "Faltando o 'phone' no usuario: " + user);
            assertTrue(user.containsKey("username"), "Faltando o 'username' no usuario: " + user);
            assertTrue(user.containsKey("password"), "Faltando o 'password' no usuario: " + user);
            assertTrue(user.containsKey("birthDate"), "Faltando o 'birthDate' no usuario: " + user);
            assertTrue(user.containsKey("image"), "Faltando o 'image' no usuario: " + user);
            assertTrue(user.containsKey("bloodGroup"), "Faltando o  'bloodGroup' no usuario: " + user);
            assertTrue(user.containsKey("height"), "Faltando o 'height' no usuario: " + user);
            assertTrue(user.containsKey("weight"), "Faltando o 'weight' no usuario: " + user);
            assertTrue(user.containsKey("eyeColor"), "Faltando o 'eyeColor' no usuario: " + user);
            assertTrue(user.containsKey("hair"), "Faltando o 'hair' no usuario: " + user);
            assertTrue(user.containsKey("domain"), "Faltando o 'domain' no usuario: " + user);
            assertTrue(user.containsKey("ip"), "Faltando o 'ip' no usuario: " + user);
            assertTrue(user.containsKey("address"), "Faltando o 'address' no usuario: " + user);
            assertTrue(user.containsKey("macAddress"), "Faltando o  'macAddress' no usuario: " + user);
            assertTrue(user.containsKey("university"), "Faltando o 'university' no usuario: " + user);
            assertTrue(user.containsKey("bank"), "Faltando o 'bank' no usuario: " + user);
            assertTrue(user.containsKey("company"), "Faltando o 'company' no usuario: " + user);
            assertTrue(user.containsKey("ein"), "Faltando o 'ein' no usuario: " + user);
            assertTrue(user.containsKey("ssn"), "Faltando o 'ssn' no usuario: " + user);
            assertTrue(user.containsKey("userAgent"), "Faltando o 'userAgent' no usuario: " + user);

            Map<String, ?> hair = (Map<String, ?>) user.get("hair");
            assertTrue(hair.containsKey("color"), "Faltando o 'hair.color' no usuario: " + user);
            assertTrue(hair.containsKey("type"), "Faltando o 'hair.type' no usuario: " + user);

            Map<String, ?> address = (Map<String, ?>) user.get("address");
            assertTrue(address.containsKey("address"), "Faltando o 'address.address' no usuario: " + user);
            assertTrue(address.containsKey("city"), "Faltando o 'address.city' no usuario: " + user);
            assertTrue(address.containsKey("coordinates"), "Faltando o 'address.coordinates' no usuario: " + user);
            assertTrue(address.containsKey("postalCode"), "Faltando o 'address.postalCode' no usuario: " + user);
            assertTrue(address.containsKey("state"), "Faltando o 'address.state' no usuario: " + user);

            Map<String, ?> bank = (Map<String, ?>) user.get("bank");
            assertTrue(bank.containsKey("cardExpire"), "Faltando o 'bank.cardExpire' no usuario: " + user);
            assertTrue(bank.containsKey("cardNumber"), "Faltando o 'bank.cardNumber' no usuario: " + user);
            assertTrue(bank.containsKey("cardType"), "Faltando o 'bank.cardType' no usuario: " + user);
            assertTrue(bank.containsKey("currency"), "Faltando o 'bank.currency' no usuario: " + user);
            assertTrue(bank.containsKey("iban"), "Faltando o 'bank.iban' no usuario: " + user);

            Map<String, ?> company = (Map<String, ?>) user.get("company");
            assertTrue(company.containsKey("address"), "Faltando o 'company.address' no usuario: " + user);
            assertTrue(company.containsKey("department"), "Faltando o 'company.department' no usuario: " + user);
            assertTrue(company.containsKey("name"), "Faltando o 'company.name' no usuario: " + user);
            assertTrue(company.containsKey("title"), "Faltando o 'company.title' no usuario: " + user);

            Map<String, ?> companyAddress = (Map<String, ?>) company.get("address");
            assertTrue(companyAddress.containsKey("address"), "Faltando o 'company.address.address' no usuario: " + user);
            assertTrue(companyAddress.containsKey("city"), "Faltando o 'company.address.city' no usuario: " + user);
            assertTrue(companyAddress.containsKey("coordinates"), "Faltando o 'company.address.coordinates' no usuario: " + user);
            assertTrue(companyAddress.containsKey("postalCode"), "Faltando o 'company.address.postalCode' no usuario: " + user);
            assertTrue(companyAddress.containsKey("state"), "Faltando o 'company.address.state' no usuario: " + user);
        }
    }

    @Test
    @DisplayName("Validar a criação do Token para autenticação")
    public void testValidarAutenticacaoDoUsuario(){
        UserPojo usuario = new UserPojo();
        usuario.setUsername("kminchelle");
        usuario.setPassword("0lelplR");

        String token = given()
                .contentType(ContentType.JSON)
                .body(usuario)
            .when()
                .post("/auth/login")
            .then()
                .body("id", equalTo(15))
                .extract()
                .path("token");

        assertNotNull(token, "Token não gerado");
        assertFalse(token.isEmpty(), "Token vazio");
    }
}