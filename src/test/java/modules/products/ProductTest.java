package modules.products;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pojo.ProductPojo;

import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Testes de API Rest do modulo de Produto")
public class ProductTest {
    @BeforeEach
    public void beforeEach(){
        baseURI = "https://dummyjson.com";
    }

    @Test
    @DisplayName("Buscar todos os produtos")
    public void testBuscarProdutos() {
        Response response = given()
                .when()
                    .get("/products")
                .then()
                    .assertThat()
                    .statusCode(200)
                    .extract()
                    .response();

        JsonPath jsonPath = response.jsonPath();
        List<Map<String, ?>> products = jsonPath.getList("products");

        for(Map<String, ?> product : products) {
            assertTrue(product.containsKey("id"), "Faltando o 'id' no produto: " + product);
            assertTrue(product.containsKey("title"), "Faltando o 'title' no produto: " + product);
            assertTrue(product.containsKey("description"), "Faltando a 'description' no produto: " + product);
            assertTrue(product.containsKey("price"), "Faltando o 'price' no produto: " + product);
            assertTrue(product.containsKey("discountPercentage"), "Faltando o 'discountPercentage' no produto: " + product);
            assertTrue(product.containsKey("rating"), "Faltando o 'rating' no produto: " + product);
            assertTrue(product.containsKey("stock"), "Faltando o 'stock' no produto: " + product);
            assertTrue(product.containsKey("brand"), "Faltando a 'brand' no produto: " + product);
            assertTrue(product.containsKey("category"), "Faltando a 'category' no produto: " + product);
            assertTrue(product.containsKey("thumbnail"), "Faltando a 'thumbnail' no produto: " + product);
            assertTrue(product.containsKey("images"), "Faltando a 'images' no produto: " + product);
        }
    }

    @Test
    @DisplayName("Buscar apenas um produto por id")
    public void testBuscarProdutosPorID() {
        when()
            .get("/products/1")
        .then()
            .assertThat()
            .statusCode(200)
            .body("id", equalTo(1));

    }
    @Test
    @DisplayName("Teste de produto não encontrado")
    public void testProdutoNaoEncontrado(){
        given()
        .when()
            .get("/products/0")
        .then()
            .assertThat()
            .statusCode(404)
            .body("message", equalTo("Product with id '0' not found"));
    }

    @Test
    @DisplayName("Criação de produto")
    public void testCriarNovoProduto() {

        ProductPojo product = new ProductPojo();
        product.setTitle("Perfume Oil");
        product.setDescription("Mega Discount, Impression of A...");
        product.setPrice(13);
        product.setDiscountPercentage(8.4);
        product.setRating(4.26F);
        product.setStock(65);
        product.setBrand("Impression of Acqua Di Gio");
        product.setCategory("fragrances");
        product.setThumbnail("https://i.dummyjson.com/data/products/11/thumnail.jpg");

        given()
            .contentType(ContentType.JSON)
            .body(product)
        .when()
            .post("/products/add")
        .then()
            .statusCode(200)
            .body("title", equalTo("Perfume Oil"))
            .body("price", equalTo(13))
            .body("stock", equalTo(65))
            .body("rating", equalTo(4.26F))
            .body("thumbnail", equalTo("https://i.dummyjson.com/data/products/11/thumnail.jpg"))
            .body("description", equalTo("Mega Discount, Impression of A..."))
            .body("brand", equalTo("Impression of Acqua Di Gio"))
            .body("category", equalTo("fragrances"));
    }
}
