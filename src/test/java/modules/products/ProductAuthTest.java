package modules.products;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pojo.UserPojo;

import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Testes de API Rest do modulo de Produto com autenticação")
public class ProductAuthTest {
    private String token;

    @BeforeEach
    public void beforeEach(){
        baseURI = "https://dummyjson.com";
    }

    @Test
    @DisplayName("Validar a obtenção dos produtos de forma autenticada")
    public void testObterProdutosAutenticado(){
        UserPojo user = new UserPojo();
        user.setUsername("kminchelle");
        user.setPassword("0lelplR");

        token = given()
                .contentType(ContentType.JSON)
                .body(user)
                .when()
                .post("/auth/login")
                .then()
                .extract()
                .path("token");

        Response response = given()
                .contentType(ContentType.JSON)
                .header("Authorization", token)
        .when()
                .get("/auth/products")
        .then()
                .assertThat()
                .statusCode(200)
                .extract()
                .response();

        JsonPath jsonPath = response.jsonPath();
        List<Map<String, ?>> products = jsonPath.getList("products");

        for(Map<String, ?> product : products) {
            assertTrue(product.containsKey("id"), "Faltando o 'id' no produto: " + product);
            assertTrue(product.containsKey("title"), "Faltando o 'title' no produto: " + product);
            assertTrue(product.containsKey("description"), "Faltando a 'description' no produto: " + product);
            assertTrue(product.containsKey("price"), "Faltando o 'price' no produto: " + product);
            assertTrue(product.containsKey("discountPercentage"), "Faltando o 'discountPercentage' no produto: " + product);
            assertTrue(product.containsKey("rating"), "Faltando o 'rating' no produto: " + product);
            assertTrue(product.containsKey("stock"), "Faltando o 'stock' no produto: " + product);
            assertTrue(product.containsKey("brand"), "Faltando a 'brand' no produto: " + product);
            assertTrue(product.containsKey("category"), "Faltando a 'category' no produto: " + product);
            assertTrue(product.containsKey("thumbnail"), "Faltando a 'thumbnail' no produto: " + product);
            assertTrue(product.containsKey("images"), "Faltando a 'images' no produto: " + product);
        }
    }

    @Test
    @DisplayName("Validar se o token do usuário é inválido")
    public void testVerificarTokenInvalido(){
        token = "LoremipsumTokenInvalid";
            given()
                .contentType(ContentType.JSON)
                .header("Authorization", token)
            .when()
                .get("/auth/products")
            .then()
                .assertThat()
                .statusCode(401)
                .body("message", equalTo("Invalid/Expired Token!"));
    }

    @Test
    @DisplayName("Validar se da erro quando as credenciais são inválidas")
    public void testVerificarCredenciaisInvalidas(){

        given()
            .header("Content-Type", "application/json")
        .when()
            .get("/auth/products")
        .then()
            .assertThat()
            .statusCode(403) // Verifique se o status code é 401
            .body("message", equalTo("Authentication Problem"));
    }
}

