# Teste de automação de API - SICREDI
Esse projeto tem como objetivo testar de forma automatizada as requisições para a API alvo de teste no projeto Sicredi.
As informações sobre os endpoints e as instruções para o teste podem ser encontradas nesse link: https://sicredi-desafio-qe.readme.io/reference/home

## Dependências
- Rest-Assured: 5.3.1
- JUnit-Jupiter: 5.10.0
- Jackson-Databind 2.15.2

As dependencias estão inseridas no arquivo pom.xml localizado na raiz do projeto.

## Como rodar o projeto
Único requisito para que o projeto rode de maneira adequada é ter o Java JDK e o Maven instalado corretamente na máquina onde o projeto irá rodar.
- Clone esse repositório onde o projeto irá rodar
- Abra o projeto em sua IDE de preferência (recomendado o Intellij idea)
- Vá até o caminho src/test/java 
- Clique com o botão direito na pasta java, e clique em Run 'Tests in 'java''.
- Se preferir pelo terminal, rode o comando 'mvn test' na raiz do projeto


## Plano e estratégia de testes
**GET /test**   
- Comparar se o Status Code é 200;   
- Comparar se a propriedade "status" do body da resposta é igual a "ok";  
- Comparar se a propriedade "method" do body da resposta é igual a "GET";  

**GET /users**  
- Verificar se o Status Code é 200 se a requisição foi enviada com sucesso;  
- Verificar se todos os usuários retornados tem a mesma estrutura e as mesmas propriedades iterando em cada usuário;

**POST /auth/login**   
- Verificar se o Status Code é 200 se a requisição foi enviada com sucesso;
- Verificar se o usuário usado no teste tem de fato o seu ID correto no *body* da resposta;
- Verificar se o token foi gerado e se ele não é vazio;

**GET /auth/products**    
- Verificar se o Status Code é 200 se a requisição foi enviada com sucesso;
- Validar se todos os produtos tem a mesma estrutura e propriedades iterando em cada um deles;    
- Validar se o Status Code é 401 em caso de token inválido, verificando a mensagem de erro;
- Validar se o Status Code é 403 em caso de problema com autenticação, verificando a mensagem de erro;
 
**POST /products/add**          
- Verificar se o Status Code é 200 se a requisição foi enviada com sucesso;
- Comparar o *body* da resposta com o que foi enviado na requisição, confirmando que os dados são os mesmos;    

**GET /products**         
- Verificar se o Status Code é 200 se a requisição foi enviada com sucesso;
- Validar se todos os produtos tem a mesma estrutura e propriedades iterando em cada um deles;    

**GET /products/{id}**      
- Verificar se o Status Code é 200 se a requisição foi enviada com sucesso;    
- Verificar se o produto usado no teste tem de fato o seu ID correto;
- Verificar o Status Code e a mensagem de erro em caso do produto não ser encontrado. 

## Bugs identificados    
- A requisição pra adicionar um produto não precisa de token de autenticação, o que é uma falha de segurança pois como essa é uma funcionalidade importante, somente usuários autenticados deveriam conseguir faze-la.
- Está faltando o campo "city" para o usuário de ID = 12. Na nota do desafio para essa requisição dizia que os campos importantes para o negócio são username e password, porém, eu fui além e resolvi validar toda a estrutura da requisição partindo do pressuposto que todos os campos são obrigatórios, mas sabendo quais os campos são opcionais daria para adaptar o teste de acordo com os campos opcionais. Validando a estrutura da resposta o meu teste pegou que para o usuário de ID = 12 não temos o campo "city", e por isso, o teste está falhando pois é esperado que todos usuários tenham a mesma estrutura. Caso essa validação não seja de fato necessária, basta comentar a linha 98 do arquivo UserTest e rodar o teste, que ele irá passar sem problemas.
- A requisição GET para o endpoint /users é feita sem autenticação de nenhum usuário, e exibe todos os dados dos usuários da API, como senhas, emails, endereços e IPs.
- Se a requisição para adicionar produto é enviada sem alguma informação, como o "title" por exemplo, o produto é criado mesmo assim, pois não existe validação.
- Existem divergencias no Status Code que consta na documentação dos endpoints em relação ao que vem na resposta da requisição. Um exemplo é o endpoint /auth/users que na documentação consta a resposta 201 mas na prática retorna 200. O mesmo serve para o endpoint /products/add.

## Melhorias identificadas
- Existem alguns dados que não estão com o tipo correto, um exemplo disso é o "price" que está como inteiro, sendo que para informar um preço de um determinado produto muitas vezes precisamos de mais casas decimais, como é o caso da gasolina por exemplo.
- As informações na resposta da requisição para adicionar produto não estão ordenadas de uma maneira que faça sentido, um exemplo é a description estar relativamente longe do title, sendo que normalmente a description vem logo após o title por convenção.
- Existem duas requisições do tipo GET para o endpoint de produtos, sendo uma delas necessário autenticação via token, e a outra não. Sendo que isso não faz sentido em um cenário onde temos a mesma resposta para ambas requisições.

